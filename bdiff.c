
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <inttypes.h>
#include <assert.h>
#include <string.h>
#include <stdbool.h>

#include <defines/argv0.h>

#include <debug.h>

enum
{
	e_success,
	e_syscall_failed,
	e_bad_command_line_args,
	e_out_of_memory,
};

int smalloc(void** ptr, size_t size)
{
	int error = 0;
	
	void* newptr = malloc(size);
	
	if (size > 0 && newptr == NULL)
		perror("realloc"),
		error = e_out_of_memory;
	
	if (!error)
		*ptr = newptr;
	
	return error;
}

int srealloc(void** ptr, size_t size)
{
	int error = 0;
	
	void* oldptr = *ptr;
	void* newptr = realloc(oldptr, size);
	
	if (size > 0 && newptr == NULL)
		perror("realloc"),
		error = e_out_of_memory;
	
	if (!error)
		*ptr = newptr;
	
	return error;
}

static const char printable[] = ""
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	"abcdefghijklmnopqrstuvwxyz"
	"`1234567890-="
	"~!@#$%^&*()_+"
	"{}|"
	"[]\\"
	";',./"
	":\"<>?"
	" ";

#if 0
char f(uint8_t b)
{
	return memchr(printable, b, sizeof(printable) - 1) ? b : '.';
}
#endif

struct data
{
	uint8_t* data;
	size_t size;
};

static int read_file(struct data* out, const char* path)
{
	int error = 0;
	int fd = -1;
	struct stat statbuf;
	size_t size;
	uint8_t* data = NULL;
	ENTER;
	
	dpvs(path);
	
	if ((fd = open(path, O_RDONLY)) < 0)
		fprintf(stderr, "%s: open(\"%s\"): %m\n", argv0, path),
		error = e_bad_command_line_args;
	else if (fstat(fd, &statbuf) < 0)
		fprintf(stderr, "%s: fstat(\"%s\"): %m\n", argv0, path),
		error = e_syscall_failed;
	else if (!(data = malloc(size = statbuf.st_size)))
		fprintf(stderr, "%s: malloc(%lu): %m\n", argv0, size),
		error = e_out_of_memory;
	else if (read(fd, data, size) < size)
		fprintf(stderr, "%s: read(\"%s\"): %m\n", argv0, path),
		error = e_syscall_failed;
	else
	{
		out->data = data, data = NULL;
		out->size = size;
	}
	
	if (fd > 0)
		close(fd);
	
	free(data);
	
	EXIT;
	return error;
}

#if DEBUGGING
int debug_depth;
#endif

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MIN3(a, b, c) MIN(MIN(a, b), c)

int main(int argc, char* const* argv)
{
	int error = 0;
	int opt;
	struct {
		unsigned insert;
		unsigned delete;
		unsigned change;
	} costs = {1, 1, 1};
	bool show_operations = false;
	ENTER;
	
	while (!error && (opt = getopt(argc, argv, "oi:d:c:")) > 0)
	{
		switch (opt)
		{
			case 'o':
				show_operations = true;
				break;
			
			case 'i':
				TODO; // costs.insertion = atoi(optarg);
				break;
			
			case 'd':
				TODO; // costs.delete = atoi(optarg);
				break;
			
			case 'c':
				TODO; // costs.change = atoi(optarg);
				break;
			
			default:
				fprintf(stderr, "%s: unknown option '%c'\n", argv0, opt);
				error = e_bad_command_line_args;
				break;
		}
	}
	
	if (!error)
	{
		dpvb(show_operations);
	}
	
	if (!error && optind + 2 < argc)
	{
		fprintf(stderr, "%s: missing paths!\n", argv0);
		error = e_bad_command_line_args;
	}
	
	struct data first = {NULL, 0}, second = {NULL, 0};
	
	if (!error)
		error = 0
			?: read_file(&first,  argv[optind++])
			?: read_file(&second, argv[optind++]);
	
	#define matrix (*_lm)
	unsigned matrix[first.size + 1][second.size + 1] = NULL;
	
	if (!error)
		error = smalloc((void**) &_lm, sizeof(*_lm));
	
	struct {
		unsigned insert;
		unsigned delete;
		unsigned change;
	} ele;
	size_t i, n, j, m, k, l;
	unsigned total_cost;
	
	if (!error)
	{
		matrix[0][0] = 0;
		
		for (i = 1, n = first.size; i <= n; i++)
			matrix[i][0] = matrix[i - 1][0] + costs.insert;
		
		for (j = 1, m = second.size; j <= m; j++)
			matrix[0][j] = matrix[0][j - 1] + costs.delete;
		
		for (i = 1; i <= n; i++)
		for (j = 1; j <= m; j++)
		{
			ele.insert = matrix[i - 1][j - 0] + costs.insert;
			ele.delete = matrix[i - 0][j - 1] + costs.delete;
			ele.change = matrix[i - 1][j - 1] + costs.change;
			
			if (first.data[i - 1] == second.data[j - 1])
				ele.change -= ele.change;
			
			matrix[i][j] = MIN3(ele.insert, ele.delete, ele.change);
		}
		
		total_cost = matrix[first.size][second.size];
		
		dpv(total_cost);
	}
	
	TODO;
	#if 0
	enum operation
	{
		insert,
		delete,
		substitute
	};
	
	struct {
		enum  operation operation;
		union {
			struct { uint8_t insertme; };
			struct { uint8_t deleteme; };
			struct { uint8_t findme, replacewith; };
		};
		off_t location;
	} (*operations)[total_cost] = NULL;
	
	if (!error)
		error = smalloc((void**) &operations, sizeof(*operations));
	
	int n_options;
	struct {
		enum  operation operation;
		unsigned int cost;
		struct {int i, j; } new;
	} options[3], *best;
	
	if (!error)
	{
		for (i = first.n, j = second.n, k = total_cost; i > 0 || j > 0; )
		{
			n_options = 0;
			
			if (i > 0)
				options[n_options++] = (typeof(options[0])) {
					.operation = delete,
					.new.i = i - 1,
					.new.j = j,
					.cost = matrix[i - 1][j]
				};
				
			if (j > 0)
				options[n_options++] = (typeof(options[0])) {
					.operation = insert,
					.new.i = i,
					.new.j = j - 1,
					.cost = matrix[i][j - 1]
				};
			
			if (i > 0 && j > 0)
				options[n_options++] = (typeof(options[0])) {
					.operation = substitute,
					.new.i = i - 1,
					.new.j = j - 1,
					.cost = matrix[i - 1][j - 1]
				};
			
			assert(n_options > 0);
			
			for (best = &options[n_options - 1]; n_options--; )
				if (best->cost > options[n_options].cost)
					best = &options[n_options];
			
			switch (best->operation)
			{
				case insert:
					k--;
/*					printf("Inserting %c into first\n", second.data[j - 1]);*/
					(*operations)[k].operation = insert;
					(*operations)[k].insertme = second.data[j - 1];
					(*operations)[k].location = i;
					break;
				
				case delete:
					k--;
/*					printf("Deleting %c from first\n", first.data[i - 1]);*/
					(*operations)[k].operation = delete;
					(*operations)[k].deleteme = first.data[i - 1];
					(*operations)[k].location = i - 1;
					break;
				
				case substitute:
					if (first.data[i - 1] != second.data[j - 1])
					{
						k--;
/*						printf("Replacing %c from first with %c\n", first.data[i - 1], second.data[j - 1]);*/
						(*operations)[k].operation = substitute;
						(*operations)[k].findme = first.data[i - 1];
						(*operations)[k].replacewith = second.data[j - 1];
						(*operations)[k].location = i - 1;
					}
					break;
			}
			
			i = best->new.i;
			j = best->new.j;
		}
	}
	
	struct { off_t i, j; } offsets = {0, 0};
	char hex_buffer[40 * 16 + 1] , *hmoving;
	char ascii_buffer[20 * 16 + 1] , *amoving;
	char operations_buffer[20 * 16 + 1] , *omoving;
	
	#define INS "\e[32m"
	#define DEL "\e[31m"
	#define SUB "\e[34m"
	#define RES "\e[0m"
	
	hmoving = hex_buffer, amoving = ascii_buffer, omoving = operations_buffer;
	
	bool color = isatty(1);
	
	if (!error)
	for (l = 0, i = 0, j = 0, k = 0,
		n = first.n, m = second.n; i < n || j < m; )
	{
		if (k < total_cost && (*operations)[k].location == i)
		{
			switch ((*operations)[k].operation)
			{
				case insert:
					byte = (*operations)[k++].insertme;
					
					if (color)
					{
						hmoving += sprintf(hmoving, "%s", INS);
						amoving += sprintf(amoving, "%s", INS);
						omoving += sprintf(omoving, "%s", INS);
					}
					
					hmoving += sprintf(hmoving, " %02x", byte);
					amoving += sprintf(amoving, "%c", f(byte));
					omoving += sprintf(omoving, "I");
					
					i += 0, j += 1;
					break;
				
				case delete:
					byte = (*operations)[k++].deleteme;
					
					if (color)
					{
						hmoving += sprintf(hmoving, "%s", DEL);
						amoving += sprintf(amoving, "%s", DEL);
						omoving += sprintf(omoving, "%s", DEL);
					}
					
					hmoving += sprintf(hmoving, " %02x", byte);
					amoving += sprintf(amoving, "%c", f(byte));
					omoving += sprintf(omoving, "D");
					
					i += 1, j += 0;
					break;
				
				case substitute:
					byte = (*operations)[k++].replacewith;
					
					if (color)
					{
						hmoving += sprintf(hmoving, "%s", SUB);
						amoving += sprintf(amoving, "%s", SUB);
						omoving += sprintf(omoving, "%s", SUB);
					}
					
					hmoving += sprintf(hmoving, " %02x", byte);
					amoving += sprintf(amoving, "%c", f(byte));
					omoving += sprintf(omoving, "S");
					
					i += 1, j += 1;
					break;
			}
			
			if (color)
			{
				hmoving += sprintf(hmoving, "%s", RES);
				amoving += sprintf(amoving, "%s", RES);
				omoving += sprintf(omoving, "%s", RES);
			}
		}
		else
		{
			byte = first.data[j++, i++],
			hmoving += sprintf(hmoving, " %02x", byte),
			amoving += sprintf(amoving, "%c", f(byte));
			omoving += sprintf(omoving, "-");
		}
		
		if (++l == 16 || (i == n && j == m))
		{
			printf("%08lx/%08lx %s%*s  |%s|%*s",
				offsets.i, offsets.j,
				hex_buffer,
				(int) ((16 - l) * 3), "",
				ascii_buffer,
				(int) (16 - l), "");
			
			if (!color || show_operations)
				printf(" |%s|\n", operations_buffer);
			else
				printf("\n");
			
			hmoving = hex_buffer;
			amoving = ascii_buffer;
			omoving = operations_buffer;
			l = 0;
			offsets.i = i, offsets.j = j;
		}
	}
	
	if (!error)
		printf("%08lx/%08lx\n", i, j);
	
	if (firstfd > 0) close(firstfd);
	if (secondfd > 0) close(secondfd);
	
	free(first.data), free(second.data);
	
	free(operations);
	
	free(levenshtein_matrix);
	#endif
	TODO;
	
	EXIT;
	return error;
}



















