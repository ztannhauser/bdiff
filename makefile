
SHELL = bash

BUILDTYPE = release

CC = gcc

CFLAGS += -Wall -Werror

CPPFLAGS += -I .
CPPFLAGS += -D _GNU_SOURCE

ifeq ($(BUILDTYPE), release)
CPPFLAGS += -D DEBUGGING=0

CFLAGS += -O2
CFLAGS += -flto
else
CPPFLAGS += -D DEBUGGING=1

CFLAGS += -g
CFLAGS += -Wno-unused-variable
CFLAGS += -Wno-unused-function
CFLAGS += -Wno-unused-but-set-variable
endif

default: bin/bdiff

bin/:
	mkdir -p $@

SRCS += bdiff.c

OBJS = $(patsubst %.c,bin/%.$(BUILDTYPE).o,$(SRCS))

/tmp/a:
	echo levenshtein > $@

/tmp/b:
	echo meilenstein > $@

run: bin/bdiff.$(BUILDTYPE) /tmp/a /tmp/b
	$< ${ARGS} /tmp/a /tmp/b

valrun: bin/bdiff.$(BUILDTYPE) /tmp/a /tmp/b
	valgrind $< ${ARGS} /tmp/a /tmp/b

install: ~/bin/bdiff

~/bin/bdiff: bin/bdiff
	install $< -D $@

bin/bdiff.$(BUILDTYPE): $(OBJS) | bin/
	$(CC) $(LDFLAGS) $^ $(LOADLIBES) $(LDLIBS) -o $@

bin/%.$(BUILDTYPE).o: %.c | bin/
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -o $@



